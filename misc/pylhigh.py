#!/usr/bin/python3 -B

import argparse
import sys
# ruamel.yaml test of ROUND TRIP
import ruamel.yaml as ruaml

def process(yaml):
	s = str(yaml)
	return s

def main():
	parser = argparse.ArgumentParser(description = "Print all the content of a yaml file")
	parser.add_argument('fnyaml', type = str, help = 'filename.yaml')
	args = parser.parse_args()
	
	fyaml = open(args.fnyaml, 'r')
	ystream = ruaml.YAML(pure = True, typ = 'safe')
	# ystream = ruaml.YAML(pure = True, typ = 'safe')
	# yaml.allow_duplicate_keys = True
	# ystream = ruaml.safe_load_all(fyaml)
	# yaml = ruaml.load_all(fyaml)
	ystream = ruaml.round_trip_load_all(fyaml)
	
	for ydoc in ystream:
		print('\n---')
		print('# PRINT YAML DOC')
		print(ydoc)
		
		print('\n# ROUND TRIP DUMP YAML DOC')
		# ruaml.dump(ydoc, sys.stdout)
		ruaml.round_trip_dump(ydoc, sys.stdout)
		
		print('\n# PROCESS YAML DOC')
		print(process(ydoc))
	print('...')
	
	fyaml.close()

if __name__ == '__main__':
	main()
