#!/usr/bin/python3 -B

import argparse
import sys
# Using another yaml python library
import yaml

def process(ydata):
	s = str(ydata)
	return s

def main():
	parser = argparse.ArgumentParser(description = "Print all the content of a yaml file")
	parser.add_argument('fnyaml', type = str, help = 'filename.yaml')
	args = parser.parse_args()
	
	fyaml = open(args.fnyaml, 'r')
	ydata = yaml.safe_load_all(fyaml)
	
	for d in ydata:
		print('\n---')
		print('# PRINT YAML')
		print(d)
		
		print('\n# DUMP')
		yaml.dump(d, sys.stdout)
		
		print('\n# PROCESS')
		print(process(d))
	print('...')
	
	fyaml.close()
	
	# isinstance(x, str)

if __name__ == '__main__':
	main()
