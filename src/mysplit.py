#!/usr/bin/env python3

"""
myproc conf.meta.yaml | mysplit "ydoc['exp']['id'] + '__' + ydoc['inst']['id'] + '__' + ydoc['test']['id'] + '.test.conf.yaml'" -s
Will split the yaml file in docs and write a file with this name.
d is the dir
"""

import sys
import argparse
import os
import ruamel.yaml
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../intern')
import utilyq

def drive():
	parser = argparse.ArgumentParser(description="Yaml file splitter")
	parser.add_argument('patternoutyamlfns', type=str, default=None, help='The pattern of the output yaml filenames in Python 3 sintax. One can assume the existence of the yaml variable ydoc, which represents each of the multi yaml docs in the input.')
	parser.add_argument('inmultdocsyamlfn', nargs='?', type=str, default=None, help='The input multdocs yaml filename with to be splitted. If empty, the program will read data from stdin in a non blocking way (this only works in Linux systems).')
	parser.add_argument('-d', '--outdir', type=str, default='.', help='The directory where is to store the output yaml files generated. The default behaviour is to use current dir.')
	parser.add_argument('-n', '--dontwriteoutfns', action='store_true', help='Do not write in stdout the filenames of the output yaml files generated')
	parser.add_argument('-m', '--omitdir', action='store_true', help='Omit directory in writing the filenames')
	parser.add_argument('-v', '--verb', action='store_true', help='Very verbose mode for debug purposes')
	parser.add_argument('-s', '--patternoutsubdir', type=str, default=None, help='The pattern of the output subdir. One can assume the existence of the yaml variable ydoc, which represents each of the multi yaml docs in the input.')

	params = parser.parse_args()

	utilyq.g_verb = params.verb

	if params.inmultdocsyamlfn is not None and not sys.stdin.isatty():
		sys.exit('Error: main: there are a input yaml filename and data in stdin (user must supply exactly one)')
	elif params.inmultdocsyamlfn is not None:
		file = open(params.inmultdocsyamlfn, 'r')
	elif not sys.stdin.isatty():
		file = sys.stdin
	else:
		sys.exit('Error: main: there is no input yaml filename nor stdin data')

	stream = ruamel.yaml.round_trip_load_all(file)

	i = 0
	for ydoc in stream:
		utilyq.verb('params.patternoutyamlfns', params.patternoutyamlfns)
		utilyq.verb('params.patternoutsubdir', params.patternoutsubdir)

		patternoutsubdir = eval(params.patternoutsubdir, None, locals())
		if patternoutsubdir:
			outcompldir = params.outdir + '/' + patternoutsubdir
		else:
			outcompldir = params.outdir

		if not os.path.exists(outcompldir):
			os.makedirs(outcompldir)

		outyamlfn = eval(params.patternoutyamlfns, None, locals())

		ydocfile = open(outcompldir + '/' + outyamlfn, 'w+')

		ruamel.yaml.round_trip_dump(ydoc, ydocfile, default_flow_style=False, line_break=1, explicit_start=True, explicit_end=True, version=(1,2), indent=4, top_level_colon_align=False)

		ydocfile.close()

		if not params.dontwriteoutfns:
			if outcompldir != '.' and not params.omitdir:
				print(outcompldir + '/' + outyamlfn)
			else:
				print(outyamlfn)

		i += 1

if __name__ == '__main__':
	drive()
