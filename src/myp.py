#!/usr/bin/env python3

"""
https://stackoverflow.com/questions/44390818/inserting-a-key-value-pair-at-specified-position-in-a-python-dictionary-loaded-f
https://stackoverflow.com/questions/46737550/inserting-node-in-yaml-with-ruamel
"""

import argparse
import sys
import collections.abc
import copy
import ruamel.yaml
import os
import re

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../intern')
import utilyq


g_modcounter = 0
g_firstmodcounter = 0


g_modcounter_singl = 0


def iszmap(x, z):
	return len(x) > 0 and next(iter(x.items())) == (z, None)


def iszomap(x, z):
	return len(x) > 0 and next(iter(x.items())) == (0, z)


def iszom(x, z):
	return iszmap(x, z) or iszomap(x, z)


def adjzmap(x):
	# return a standard meta map, ie without the first ~: ~ item. Algo checks the meta map
	zmap = collections.abc.OrderedDict(list(x.items())[1:])
	for k in zmap:
		if zmap[k] is not None:
			utilyq.fail('Error: adjzmap')
	return zmap


def adjzomap(x):
	# return a standard meta omap, ie without the first 0: ~ item  # Also checks the meta omap
	utilyq.verb('x', x)
	zomap = collections.abc.OrderedDict((v, None) for (i, v) in list(x.items())[1:])  # collections.abc.OrderedDict([(v, None) for (i, v) in x.values()][1:])
	utilyq.verb('zomap', zomap)
	#ok = 1
	#for k in zomap.items():
	#	if k != ok:
	#		utilyq.fail('Error')
	#	ok += 1
	return zomap


def adjzom(x, z):
	if iszmap(x, z):
		return adjzmap(x)
	elif iszomap(x, z):
		return adjzomap(x)
	else:
		return x


def iszseq(x, z):
	return len(x) > 0 and x[0] == z


def calcnumnextnewdocs(x=None, startkeyidx=None, z=None):
	#utilyq.verb('CALC x', x, 'startkeyidx', startkeyidx)

	numnextnewdocs = 1

	# SCALAR  # OK
	if isinstance(x, str) or x is None or not isinstance(x, collections.abc.Collection):
		utilyq.verb('CALC / SCALAR / TYPE', str(type(x)), str(x), 'startkeyidx', startkeyidx)

	# MAP or OMAP  # OK
	elif isinstance(x, (ruamel.yaml.comments.CommentedMap, ruamel.yaml.comments.CommentedKeyMap, ruamel.yaml.comments.CommentedOrderedMap, collections.abc.Mapping)):
		utilyq.verb('CALC / MAP or OMAP / TYPE', str(type(x)), '\n', str(x), 'startkeyidx', startkeyidx)
		effect = False
		for k in x:
			numnextnewdocs *= calcnumnextnewdocs(x=k)  # processing keys
			numnextnewdocs *= calcnumnextnewdocs(x=x[k])  # process vals
			if k == startkeyidx:
				effect = True
			if effect:
				numnextnewdocs *= calcnumnextnewdocs(x=k)  # processing keys
				numnextnewdocs *= calcnumnextnewdocs(x=x[k])  # process vals

	# ZSEQ  # OK
	elif isinstance(x, (ruamel.yaml.comments.CommentedSeq, ruamel.yaml.comments.CommentedKeySeq, collections.abc.Sequence))  and iszseq(x, z):
		utilyq.verb('CALC / ZSEQ / TYPE', str(type(x)), '\n', str(x), 'startkeyidx', startkeyidx)
		if len(x) >= 2:
			numnextnewdocs *= len(x) - 1
			first = True
			effect = startkeyidx is None
			for i, k in enumerate(x):
				if k == startkeyidx:
					effect = True
				if not first and effect:
					numnextnewdocs *= calcnumnextnewdocs(x=k)
				else:
					first = False

	# SEQ  # OK
	elif isinstance(x, (ruamel.yaml.comments.CommentedSeq, ruamel.yaml.comments.CommentedKeySeq, collections.abc.Sequence)):
		utilyq.verb('CALC / SEQ / TYPE', str(type(x)), '\n', str(x), 'startkeyidx', startkeyidx)
		effect = startkeyidx is None
		for i, k in enumerate(x):
			if k == startkeyidx:
				effect = True
			if effect:
				numnextnewdocs *= calcnumnextnewdocs(x=k)

	# SET  # OK
	elif isinstance(x, (ruamel.yaml.comments.CommentedSet, collections.abc.Set)):
		utilyq.verb('CALC / SET / TYPE', str(type(x)), '\n', str(x), 'startkeyidx', startkeyidx)
		effect = startkeyidx is None
		for k in x:
			if k == startkeyidx:
				effect = True
			if effect:
				numnextnewdocs *= calcnumnextnewdocs(x=k)

	utilyq.verb('numnextnewdocs', numnextnewdocs)

#	if isinstance(varparenx, collections.abc.Mapping):
#		for k, v in varparenx.items():
#			#print('k', k, 'v', v)
#			numnextnewdocs *= calcnumnextnewdocs(k, varparenx, varxkeyidx, varxkeyidx, isvarxakey=True)
#			numnextnewdocs *= calcnumnextnewdocs(v, varparenx, varxkeyidx, varxkeyidx)
#	elif isinstance(varparenx, collections.abc.Sequence):
#		for i in varparenx:
#			#print('i', i)
#			numnextnewdocs *= calcnumnextnewdocs(i, varparenx, varxkeyidx, varxkeyidx)

	return numnextnewdocs


def procmult(constx=None, varx=None, varparenx=None, z=None, varrootx=None, isvarxmut=True, varxkeyidx=None, isvarxakey=False):

	# SCALAR
	if isinstance(constx, str) or constx is None or not isinstance(constx, collections.abc.Collection):
		utilyq.verb('MULT / SCALAR / TYPE', str(type(constx)), str(constx))
		if varx == 'MODCOUNTER':
			varx = '####'

		if bool(re.match('^#+$', str(varx))):
			numnextnewdocs = calcnumnextnewdocs(x=varparenx, startkeyidx=varxkeyidx)
			utilyq.verb('numnextnewdocs', numnextnewdocs)
			global g_firstmodcounter
			if g_firstmodcounter == 0:
				g_firstmodcounter = numnextnewdocs
				utilyq.verb('g_firstmodcounter', g_firstmodcounter)
			if numnextnewdocs == 1:
				global g_modcounter
				g_modcounter %= g_firstmodcounter
				g_modcounter += 1
				varparenx[varxkeyidx] = "'#" + utilyq.mvzeros(g_modcounter, len(str(varx)) - 1) + "'"
				utilyq.verb('varparenx[', varxkeyidx, ']', varparenx[varxkeyidx])
				utilyq.verb('varparenx mut', isinstance(varparenx, (collections.abc.MutableSequence, collections.abc.MutableSet, collections.abc.MutableMapping)))
				utilyq.verb('I varparenx', varparenx)

	# MAP or OMAP
	elif isinstance(constx, (ruamel.yaml.comments.CommentedMap, ruamel.yaml.comments.CommentedKeyMap, ruamel.yaml.comments.CommentedOrderedMap, collections.abc.Mapping)):
		utilyq.verb('MULT / MAP or OMAP / TYPE', str(type(constx)), '\n', str(constx))
		frozenvarxkeys = copy.deepcopy(varx)
		newdocs1 = []
		newdocs2 = []
		for k, j in zip(constx, frozenvarxkeys):  # for k, j in zip(constx, varx):
			newdocs1 = procmult(constx=k, varx=j, isvarxmut=False, varparenx=varx, varrootx=varrootx, varxkeyidx=j, isvarxakey=True)  # processing keys
			if newdocs1:
				return newdocs1
			utilyq.verb('BEFORE varx', varx)
			newdocs2 = procmult(constx=constx[k], varx=varx[j], varparenx=varx, varrootx=varrootx, varxkeyidx=j)  # process vals, attributing to the keys if necessary
			utilyq.verb('AFTER  varx', varx)
			utilyq.verb('newdocs2   ', newdocs2)
			if newdocs2:
				return newdocs2

	# ZSEQ
	elif isinstance(constx, (ruamel.yaml.comments.CommentedSeq, ruamel.yaml.comments.CommentedKeySeq, collections.abc.Sequence))  and iszseq(constx, z):
		utilyq.verb('MULT / ZSEQ / TYPE', str(type(constx)), '\n', str(constx))
		newdocs = []
		if isvarxakey:
			v = copy.deepcopy(varparenx[varxkeyidx])
			del varparenx[varxkeyidx]
		first = True
		for k in constx:
			if not first:
				if not isvarxakey:  # works for val and seq
					varparenx[varxkeyidx] = k
				else:
					varparenx[k] = v
				newdocs.append(copy.deepcopy(varrootx))  # copy varrootx as a newdoc
				if isvarxakey:
					del varparenx[k]
			else:
				first = False
		return newdocs

	# SEQ
	elif isinstance(constx, (ruamel.yaml.comments.CommentedSeq, ruamel.yaml.comments.CommentedKeySeq, collections.abc.Sequence)):
		utilyq.verb('MULT / SEQ / TYPE', str(type(constx)), '\n', str(constx))
		for i, k in enumerate(constx):
			newdocs = procmult(constx=constx[i], varx=varx[i], varparenx=varx, varrootx=varrootx, varxkeyidx=i)
			if newdocs:
				return newdocs

	# SET
	elif isinstance(constx, (ruamel.yaml.comments.CommentedSet, collections.abc.Set)):
		utilyq.verb('MULT / SET / TYPE', str(type(constx)), '\n', str(constx))
		if isvarxmut:
			for k in constx:
				l = copy.deepcopy(k)
				varx.remove(k)
				wrongnewdocs = procmult(constx=k, varx=l, varparenx=varx, varrootx=varrootx)
				if wrongnewdocs:
					utilyq.fail('Error: procmult: set: wrongnewdocs')  # does not accept expanding a elem
				varx.add(l)

	return []


def procsingl(constx=None, varx=None, isvarxmut=True, varparenx=None, paramnewobjs=[], z=None, ydoc=None, varxkeyidx=None, processidseq=False):  # constparenx=None, varrootx=None
	"""
	process that results in a single doc
	constx: imutable obj ie const. Directly use its children
	mutparenx: mutable obj. This is used as parent
	newkeys: keys that must have their vals filled
	z: the meta obj
	"""

	# SCALAR
	if isinstance(constx, str) or constx is None or not isinstance(constx, collections.abc.Collection):
		utilyq.verb('SINGL / SCALAR / TYPE', str(type(constx)), str(constx))

		if varx == 'MODCOUNTERSINGL':
			varx = '@@@@'

		if bool(re.match('^@+$', str(varx))):
			global g_modcounter_singl
			g_modcounter_singl %= int(str(varx).replace('@','9'))
			g_modcounter_singl += 1
			varparenx[varxkeyidx] = "'@" + utilyq.mvzeros(g_modcounter_singl, len(str(varx)) - 1) + "'"
			utilyq.verb('varparenx[', varxkeyidx, ']', varparenx[varxkeyidx])
			utilyq.verb('varparenx mut', isinstance(varparenx, (collections.abc.MutableSequence, collections.abc.MutableSet, collections.abc.MutableMapping)))
			utilyq.verb('I varparenx', varparenx)

		for k in paramnewobjs:  # enters only if there are paramnewobjs
			varparenx[k] = varx  # constx  # ie put copies of the same val in the already added keys  # SCALAR will always have a parent

	# ZMAP or ZOMAP
	elif isinstance(constx, (ruamel.yaml.comments.CommentedMap, ruamel.yaml.comments.CommentedKeyMap, ruamel.yaml.comments.CommentedOrderedMap, collections.abc.Mapping)) and iszom(constx, z):
		# if paramnewobjs is []: processing keys
		# if paramnemobjs is not []: processing values
		utilyq.verb('SINGL / ZMAP or ZOMAP / TYPE', str(type(constx)), '\n', str(constx))
		utilyq.verb('paramnewobjs', paramnewobjs)
		if paramnewobjs and isinstance(varparenx, collections.abc.Sequence):
			utilyq.fail('Error: procsingl: zmap or zomap')
		first = True
		for k, j in zip(constx, varx):
			if not first:
				wrongnewobjs = procsingl(constx=k, varx=j, isvarxmut=False, varparenx=varx, varxkeyidx=j, ydoc=ydoc, processidseq=processidseq)
				if wrongnewobjs:
					utilyq.fail('Error: procsingl: zmap or zopmap: wrongnewobjs')
			else:
				first = False
		utilyq.verb('varx', varx)
		if isinstance(varx, ruamel.yaml.comments.CommentedOrderedMap) or list(varx.keys())[0] == 0 and list(varx.values())[0] is None:  # gambis to identify !!omap as a key, as the ruaml.yaml tag it as CommentedKeyMap
			auxvarx = list(varx.values())[1:]
		else:
			auxvarx = list(varx.keys())[1:]
		if not paramnewobjs:  # processing keys
			return auxvarx  # ie return new keys
		elif len(auxvarx) == 1:  # processing values
			for k in paramnewobjs:
				varparenx[k] = auxvarx[0]  # ie put copies of the same val in the already added keys
		elif len(paramnewobjs) == len(auxvarx):  # processing values
			for l, v in zip(paramnewobjs, auxvarx):
				varparenx[l] = v  # ie put val in the already added key
		else:
			utilyq.fail('Error: procsingl: zmap or zomap: the number of keys is incompatible with the number of values')

	# MAP or OMAP
	elif isinstance(constx, (ruamel.yaml.comments.CommentedMap, ruamel.yaml.comments.CommentedKeyMap, ruamel.yaml.comments.CommentedOrderedMap, collections.abc.Mapping)):
		utilyq.verb('SINGL / MAP or OMAP / TYPE', str(type(constx)), '\n', str(constx))
		frozenvarxkeys = copy.deepcopy(varx)
		for k, j in zip(constx, frozenvarxkeys): #for i, k in enumerate(constx):
			utilyq.verb('paramnewobjs', paramnewobjs)
			newkeys       = procsingl(constx=k, varx=j, isvarxmut=False, varparenx=varx, varxkeyidx=j, ydoc=ydoc, processidseq=processidseq)  # processing key
			utilyq.verb('newkeys', newkeys)
			wrongnewvals = procsingl(constx=constx[k], varx=varx[k], varparenx=varx, paramnewobjs=newkeys, varxkeyidx=j, ydoc=ydoc, processidseq=processidseq)  # process val, attributing to the keys if necessary
			#if not isinstance(constx, )
			utilyq.verb('wrongnewvals', wrongnewvals)
			if wrongnewvals:
				utilyq.fail('Error: procsingl: map or omap: wrongnewvals: '+ str(wrongnewvals))
			if isvarxmut:
				if newkeys:
					del varx[k]
				else:
					varx[k] = varx.pop(k)  # remove k and insert again, only to change the order in the orddict
		for k in paramnewobjs:
			varparenx[k] = varx  # ie put copies of the same val in the already added keys

	# IDSEQ
	elif processidseq and isinstance(constx, (ruamel.yaml.comments.CommentedSeq, ruamel.yaml.comments.CommentedKeySeq, collections.abc.Sequence))  and iszseq(constx, '-'):
		utilyq.verb('-')
		utilyq.verb('isvarxmut|', isvarxmut)
		utilyq.verb('varx|', varx)
		utilyq.verb('|varparenx[varxkeyidx]', varparenx[varxkeyidx])
		v = ''
		for k in varx[1:]:
			utilyq.verb('k', k)
			r = eval(k, None, locals())
			utilyq.verb('r', r)
			v += str(r)
			utilyq.verb('v', v)
		utilyq.verb('varparenx', varparenx)
		varx = v
		varparenx[varxkeyidx] = v
		utilyq.verb('|varx', varx)
		utilyq.verb('|varparenx[varxkeyidx]', varparenx[varxkeyidx])

	# SEQ
	elif isinstance(constx, (ruamel.yaml.comments.CommentedSeq, ruamel.yaml.comments.CommentedKeySeq, collections.abc.Sequence)):
		utilyq.verb('SINGL / SEQ / TYPE', str(type(constx)), '\n', str(constx))
		NEWTERMS = []
		STARTPOS = []
		for i, k in enumerate(constx):
			newterms = procsingl(constx=constx[i], varx=varx[i], varparenx=varx, varxkeyidx=i, ydoc=ydoc, processidseq=processidseq)
			if newterms:
				NEWTERMS.append(newterms)
				STARTPOS.append(i)
		NEWTERMS.reverse()
		STARTPOS.reverse()
		for i, term in zip(STARTPOS, NEWTERMS):
			del varx[i]
			for j, t in zip(range(len(term)), term):
				varx.insert(i + j, t)
		for k in paramnewobjs:
			varparenx[k] = varx  # ie put copies of the same val in the already added keys

	# SET
	elif isinstance(constx, (ruamel.yaml.comments.CommentedSet, collections.abc.Set)):
		utilyq.verb('SINGL / SET / TYPE', str(type(constx)), '\n', str(constx))
		if isvarxmut:
			for k in constx:
				l = copy.deepcopy(k)
				varx.remove(k)
				wrongnewelems = procsingl(constx=k, varx=l, varparenx=varx, ydoc=ydoc, processidseq=processidseq)
				if wrongnewelems:
					utilyq.fail('Error: procsingl: set: wrongnewelems')
				varx.add(l)
			for k in paramnewobjs:
				varparenx[k] = varx  # ie put copies of the same val in the already added keys  # SET will always have a parent

	return []


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Generate yaml files from a meta yaml file")
	parser.add_argument('inmetayamlfn', nargs='?', type=str, default=None, help='The input meta yaml filename. If empty, the program will read data from stdin in a non blocking way (this only works in Linux systems).')
	parser.add_argument('-v','--verb', action='store_true', help='Very verbose mode for debug purposes')
	parser.add_argument('-m','--msg', action='store_true', help='Show informative messages')

	params = parser.parse_args()

	utilyq.verb, utilyq.msg = utilyq.setup_msgs(params.verb, params.msg)

	if params.inmetayamlfn is not None and not sys.stdin.isatty():
		utilyq.fail('Error: main: there are input meta yaml filename and data in stdin (user must supply exactly one)')
	elif params.inmetayamlfn is not None:
		metafile = open(params.inmetayamlfn, 'r')
	elif not sys.stdin.isatty():
		metafile = 	sys.stdin
	else:
		utilyq.fail('Error: main: there is no input meta yaml filename nor stdin data')

	utilyq.verb('######\n')

	metastream = ruamel.yaml.round_trip_load_all(metafile)
	# yaml_loader = ruamel.yaml.YAML()
	# yaml_loader.allow_duplicate_keys = True
	# metastream = yaml_loader.load_all(metafile)

	utilyq.verb('### metastream ###', 'TYPE', str(type(metastream)), str(metastream))

	for metadoc in metastream:
		utilyq.msg('Processing metadoc')

		utilyq.verb('\n### metadoc (str) ###\n' + str(metadoc))
		utilyq.verb('\n### metadoc (dump) ###\n' + ruamel.yaml.round_trip_dump(metadoc))

		utilyq.verb('### PROCMULT ###')
		multdocs = list()
		multdocs.append(metadoc)
		utilyq.msg(len(multdocs), 'multdoc to be processed')
		prevlenmds = -1
		r = 0
		newmultdocs = [None]
		while newmultdocs: # len(multdocs) != prevlenmds:
			utilyq.msg('Round', r + 1, 'processing', len(multdocs), 'multdoc(s)')
			prevlenmds = len(multdocs)
			newmultdocs = []
			idxrm = []
			i = 0
			while i < len(multdocs):
				constmultdoc = copy.deepcopy(multdocs[i])
				newmultdocs += procmult(constx=constmultdoc, varx=multdocs[i], varparenx=None, z=None, varrootx=multdocs[i])
				if newmultdocs:
					idxrm.append(i)
				i += 1
			idxrm.reverse()
			for i in idxrm:
				del multdocs[i]
			multdocs += newmultdocs
			r += 1
			utilyq.msg(len(multdocs) - prevlenmds, 'multdoc(s) have been added in this round')

		utilyq.verb('### multdocs ###\n', multdocs)

		utilyq.msg('Processing', len(multdocs), 'multdoc(s) into singldoc(s)')

		for i, multdoc in enumerate(multdocs):
			utilyq.verb('### multdoc (str) ###\n', 'TYPE', str(type(multdoc)), '\n', str(multdoc))
			utilyq.verb('\n### multdoc (dump) ###\n' + ruamel.yaml.round_trip_dump(multdoc))

			utilyq.verb('### PROCSINGL ###')
			singldocnoidseq = copy.deepcopy(multdoc)
			procsingl(constx=multdoc, varx=singldocnoidseq, varparenx=None, ydoc=singldocnoidseq, processidseq=False)

			utilyq.verb('### PROCSINGL, including IDSEQ ###')
			singldoc = copy.deepcopy(singldocnoidseq)
			procsingl(constx=singldocnoidseq, varx=singldoc, varparenx=None, ydoc=singldoc, processidseq=True)

			utilyq.verb('### singldoc ###', 'TYPE', str(type(singldoc)), '\n', str(singldoc))
			utilyq.verb('\n### singldoc (via dump) ###')
			arg_explicit_end = i + 1 == len(multdocs)
			arg_version = (1,2) if i == 0 else None
			ruamel.yaml.round_trip_dump(singldoc, sys.stdout, default_flow_style=False, line_break=1, explicit_start=True, explicit_end=arg_explicit_end, version=arg_version, indent=4, top_level_colon_align=False)

		utilyq.msg(len(multdocs), 'singldoc(s) have been processed')

	utilyq.verb('\n######')

	if params.inmetayamlfn is not None:
		metafile.close()
