# TODO

- [x] make a python3 program to only read print all the content of a yaml file. It’s name’s pyl.py
- [x] !!map {~,a, b, c}: !!map {~, 1, 2, 3}
- [x] !!omap {~,a, b, c}: !!omap {~, 1, 2, 3}
- [x] if no file name as argument, catch from stdin
- [x] correct seq composed
- [x] correct set fake
- [x] improve order
- [x] include MODCOUNTER
- [x] fork ie a: !!seq [~, 1, 2, 3]
- [x] implement ysplit

# CORRETIONS

- [ ] Correct MODCOUNTER to work at exp section (for now, it its disabled)

# IMPROVEMENTS

- [x] import utilxq.py
- [x] make fields [-, ...] can now reference fields declared after them
- [ ] make option to remove comments of the generated files
    - apply before procmult
    - remove comments: https://stackoverflow.com/questions/38252507/how-can-i-get-comments-from-a-yaml-file-using-ruamel-yaml-in-python
    - remove comments: https://pypi.org/project/ruamel.yaml/0.9.7/

# ROADMAP

- [ ] enable nested zmaps or zomaps processing: super recursive
- [ ] create the user types. The prefix z means meta. The idea is to use these type instead of ~
    - [ ] !zmap, that inherits from !!map. Examp: !!map {~, a, b, c}  ->  !zmap {a, b, c}
    - [ ] !zomap that inherits from !!omap. Examp: !!omap [0: ~, 1: a, 2: b, 3: c]  ->  !zomap [0: a, 1: b, 2: c]
    - [ ] !zseq that inherits from !!seq. Examp: !!seq [~, 1, 2, 3]  ->  !zseq [1, 2, 3]
- [ ] option to read zmap as zomap
- [ ] Add MODCOUNTERB and MODCOUNTERC

# MISC

- [ ] ~~Make a c++17 example program to only read and print all the content of a yaml file~~
- [ ] ~~Correct a zseq with only one item: the trial did not detect this problem anymore~~
- [ ] ~~substitute stdzmap and stdzomap to just a deletion of the first item: it is not relevant anymore~~
